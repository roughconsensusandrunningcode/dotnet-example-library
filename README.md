A .NET example library

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=roughconsensusandrunningcode-lattuga_test&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=roughconsensusandrunningcode-lattuga_test)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=roughconsensusandrunningcode-lattuga_test&metric=coverage)](https://sonarcloud.io/summary/new_code?id=roughconsensusandrunningcode-lattuga_test)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=roughconsensusandrunningcode-lattuga_test&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=roughconsensusandrunningcode-lattuga_test)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=roughconsensusandrunningcode-lattuga_test&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=roughconsensusandrunningcode-lattuga_test)
